<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware(['auth'])->group(function(){

        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::put('/profile', 'ProfileController@update')->name('profile.update');

        Route::get('/about', function () {
            return view('about');
        })->name('about');


        // Route::get('/', function () {
        //     return view('login');
        // });


        Route::resource('/product', 'ProductController')->middleware(['Shop']);
        Route::resource('/payment', 'PaymentController');
        Route::resource('/balance', 'BalanceController')->middleware(['Bank']);


        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::put('/profile', 'ProfileController@update')->name('profile.update');

        Route::put('/product', 'ProductController@index')->name('product');
        Route::put('/product-edit/{id}', 'ProductController@edit')->name('product-edit');
        Route::get('/product-destroy/{id}', 'ProductController@destroy')->name('product-destroy');

        Route::get('/payment', 'PaymentController@index')->name('payment');
        Route::get('/payment-edit', 'PaymentController@edit')->name('payment-edit');
        Route::get('/payment-destroy/{id}', 'PaymentController@destroy')->name('payment-destroy/{id}');

        Route::get('/balance', 'BalanceController@index')->name('balance');
        Route::get('/balance-edit/{id}', 'BalanceController@edit')->name('balance-edit');
        Route::get('/balance-edit/{id}', 'BalanceController@edit')->name('balance-edit');

        Route::get('/bank', 'BalanceController@index')->name('bank')->middleware('Bank');
        Route::get('/student', 'StudentController@index')->name('student')->middleware('Student');
        Route::get('/shop', 'ShopController@index')->name('shop')->middleware('Shop');

});


// Route::get('/about', function () {
//     return view('about');
// })->name('about');

// Route::get('/blank', function () {
//     return view('blank');
// })->name('blank');

// Route::middleware('auth')->group(function() {
//     Route::resource('basic', BasicController::class);
// });

