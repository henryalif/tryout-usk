@extends('layouts.admin')

@section('main-content')

{{-- MODAL ADD ITEM --}}
<div class="modal fade" id="modalAddItem" tabindex="-1" aria-labelledby="modalAddItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAddItem">Add New Items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('product.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Item Name</label>
                        <input type="text" id="name" name="name" value="{{ old('') }}" placeholder="Insert Item Name" class="form-control">
                        {{-- @error('nama_barang') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="stocks">Stocks</label>
                        <input type="number" id="stocks" name="stocks" value="{{ old('') }}" placeholder="Insert Stocks" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" id="price" name="price" value="{{ old('') }}" placeholder="Insert Price" class="form-control">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- MODAL EDIT ITEM --}}
<div class="modal fade" id="modalEditItem" tabindex="-1" aria-labelledby="modalEditItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalEditItem">Add New Items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="formEditItem">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Item Name</label>
                        <input type="text" id="name" name="name" placeholder="Insert Item Name" class="form-control editname">
                        {{-- @error('nama_barang') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="stocks">Stocks</label>
                        <input type="number" id="stocks" name="stocks" placeholder="Insert Stocks" class="form-control editstocks">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" id="price" name="price" placeholder="Insert Price" class="form-control editprice">
                        {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                </div>
            </form>
        </div>
    </div>
</div>


    <!-- Main Content goes here -->

    <div class="card">
        <div class="card-body">
            <div class="card-header mb-3">
                <div class="row">
                    <div class="col">
                        <h2>Products List</h2>
                    </div>
                    <div class="col text-right">
                        <button type="button" href="#!" class="btn btn-primary" data-toggle="modal" data-target="#modalAddItem"><i class="fas fa-plus-circle"></i> Add Items</button>
                        </button>
                    </div>
                </div>
            </div>
            <table id="example" class="table table-striped mt-4" style="width:100%">
                <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>Item Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product as $item)
                        <tr class="text-center">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->stocks }}</td>
                            <td>{{ number_format($item->price) }}</td>
                            <td>
                                <button class="btn btn-sm btn-warning mr-1" data-id="{{ $item->id }}" onclick="triggerEdit({{ $item->id }})"><i class="fas fa-user-edit mr-1"></i></a></button>
                                <a class="btn btn-sm btn-danger ml-1" href="{{ url('/product-destroy', $item->id) }}" role="button"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>

    <!-- End of Main Content -->
@endsection

@push('notif')
@if (session('success'))
<div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (session('status'))
    <div class="alert alert-success border-left-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@endpush

@section('script')
    <script>
        $(document).ready(function() {
        $('#example').DataTable();
        } );

        function triggerEdit(id) {
            console.log("Hai")
        $.ajax({
            url: "{{ route('product.show','') }}/"+id,
            type: "GET",
            success: function(res){
                console.log(res)

                var elForm = document.querySelector("#modalEditItem form")
                elForm.setAttribute("action", "{{ url('product','') }}/"+id)
                $("#modalEditItem").modal('show');
                    console.log(res.project);
                $("#modalEditItem .editname").val(res.name);
                $("#modalEditItem .editstocks").val(res.stocks);
                $("#modalEditItem .editprice").val(res.price);
            },
            error: function(err){
                    console.log(err)
            }
        })
        }

        $(document).ready(function() {
            console.log('tes')
            $('#triggerEdit').click(function(e) {
                console.log('s');
                let id = $(this).attr('data-id');
                console.log('a');
                console.log("{{ route('product.show','') }}/" + id)
                $.get({
                    url: "{{ route('product.show','') }}/" + id,
                    data: {
                        '_token': $('meta[name=csrf-token]').attr('content'),
                    },
                    success: function(res) {
                        console.log('sukses nih');
                        console.log(res);

                        $(".editname").val(res.name);
                        $(".editstocks").val(res.stocks);
                        $(".editprice").val(res.price);
                    },
                    error: function(err) {
                        console.log('gagal');
                        console.log(err)
                    }
                })
            });
        });

    </script>

@endsection
