@extends('layouts.admin')

@section('main-content')

{{-- MODAL EDIT ITEM --}}
<div class="modal fade" id="modalEditItem" tabindex="-1" aria-labelledby="modalEditItem" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalEditItem">Top Up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="formEditItem">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="balance">Balance</label>
                        <input type="text" id="balance" name="balance" placeholder="Insert Balance" class="form-control editbalance" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-edit"></i></i> Edit</button>
                </div>
            </form>
        </div>
    </div>
</div>


    <!-- Main Content goes here -->

    <div class="card">
        <div class="card-body">
            <div class="card-header mb-3">
                <h2>User Top-Up</h2>
            </div>
            <table id="example" class="table table-striped mt-4" style="width:100%">
                <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>User Name</th>
                        <th>Class</th>
                        <th>Balance</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($get_user as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->fullname }}</td>
                        <td>{{ $item->class }}</td>
                        <td> <strong>{{ number_format($item->balance) }}</strong> </td>
                        <td>
                            <button class="btn btn-sm btn-warning mr-1" data-id="{{ $item->id }}" onclick="triggerEdit({{ $item->id }})"><i class="fas fa-user-edit mr-1"></i></a></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
      </div>

    <!-- End of Main Content -->
@endsection

@push('notif')
@if (session('success'))
<div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (session('status'))
    <div class="alert alert-success border-left-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@endpush

@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );


    function triggerEdit(id) {
        console.log("Hai")
       $.ajax({
           url: "{{ route('balance.show','') }}/"+id,
           type: "GET",
           success: function(res){
               console.log(res)

               var elForm = document.querySelector("#modalEditItem form")
               elForm.setAttribute("action", "{{ url('balance','') }}/"+id)
               $("#modalEditItem").modal('show');
                console.log(res.project);
               $("#modalEditItem .editbalance").val(res.balance);
           },
           error: function(err){
                console.log(err)
           }
       })
    }

    $(document).ready(function() {
        console.log('tes')
        $('#triggerEdit').click(function(e) {
            console.log('s');
            let id = $(this).attr('data-id');
            console.log('a');
            console.log("{{ route('balance.show','') }}/" + id)
            $.get({
                url: "{{ route('balance.show','') }}/" + id,
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                },
                success: function(res) {
                    console.log('sukses nih');
                    console.log(res);

                    $(".editbalance").val(res.balance);
                },
                error: function(err) {
                    console.log('gagal');
                    console.log(err)
                }
            })
        });
    });
</script>

@endsection
