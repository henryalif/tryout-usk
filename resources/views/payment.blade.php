@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->

    {{-- modal add payment --}}
    <div class="modal fade" id="modalAddItem" tabindex="-1" aria-labelledby="modalAddItem" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAddItem">Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('payment.store') }}" method="post">
                    @csrf
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
                    <div class="modal-body">
                        <div class="form-group" type="hidden">
                            <label for="">Items</label>
                            <select name="product_id" id="name" value="{{ old('') }}" class="form-control editItem">
                                <option selected>-- Select --</option>
                                @foreach($get_item as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty</label>
                            <input type="text" id="qty" name="qty" value="{{ old('') }}" placeholder="Insert Stocks" class="form-control">
                            {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditItem" tabindex="-1" aria-labelledby="modalEditItem" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditItem">Edit Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="post" class="formEditItem">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="form-group" type="hidden">
                            <label for="">Items</label>
                            <select name="product_id" id="name" value="{{ old('') }}" class="form-control editItem">
                                <option selected>-- Select --</option>
                                @foreach($get_item as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">Qty</label>
                            <input type="text" id="qty" name="qty" value="{{ old('') }}" placeholder="Insert Stocks" class="form-control editqty">
                            {{-- @error('harga') <code>{{ $message }}</code> @enderror --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Main Content goes here -->
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <h2>Payment</h2>
                </div>
                <div class="col text-right">
                    <button type="button" href="#!" class="btn btn-primary" data-toggle="modal" data-target="#modalAddItem"><i class="fas fa-plus-circle"></i> Add Items</button>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="table table-striped mt-4" style="width:100%">
                <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>Item Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($the_payment as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ $item->qty }}</td>
                        <td>{{ number_format($item->product->price) }}</td>
                        <td><strong> {{ number_format($item->product->price * $item->qty) }} </strong></td>
                        <td>
                            <button class="btn btn-sm btn-warning mr-1" data-id="{{ $item->id }}" onclick="triggerEdit({{ $item->id }})"><i class="fas fa-user-edit mr-1"></i></a></button>
                            <a class="btn btn-sm btn-danger ml-1" href="{{ url('/payment-destroy', $item->id) }}" role="button"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <!-- End of Main Content -->
@endsection

@push('notif')
@if (session('success'))
<div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

@if (session('status'))
    <div class="alert alert-success border-left-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@endpush

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );

    function triggerEdit(id) {
        console.log("Hai")
    $.ajax({
        url: "{{ route('payment.show','') }}/"+id,
        type: "GET",
        success: function(res){
            console.log(res)

            var elForm = document.querySelector("#modalEditItem form")
            elForm.setAttribute("action", "{{ url('payment','') }}/"+id)
            $("#modalEditItem").modal('show');
                console.log(res.project);
            $("#modalEditItem .edititem").val(res.product_id);
            $("#modalEditItem .editqty").val(res.qty);
        },
        error: function(err){
                console.log(err)
        }
    })
    }

    $(document).ready(function() {
        console.log('tes')
        $('#triggerEdit').click(function(e) {
            console.log('s');
            let id = $(this).attr('data-id');
            console.log('a');
            console.log("{{ route('payment.show','') }}/" + id)
            $.get({
                url: "{{ route('payment.show','') }}/" + id,
                data: {
                    '_token': $('meta[name=csrf-token]').attr('content'),
                },
                success: function(res) {
                    console.log('sukses nih');
                    console.log(res);

                    $(".edititem").val(res.product_id);
                    $(".editqty").val(res.qty);
                },
                error: function(err) {
                    console.log('gagal');
                    console.log(err)
                }
            })
        });
    });

</script>
