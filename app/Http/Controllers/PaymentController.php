<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $userPayment = $request->user();
        // $paymentUser = Payment::where('user_id', $userPayment->id);

        $get_item       = Product::all();
        $the_payment    = Payment::where('user_id', Auth::user()->id)->get();
        return view('payment', compact('get_item', 'the_payment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Payment::create([
            'user_id'       => $request->user()->id,
            'qty'           => $request->qty,
            'product_id'    => $request->product_id,
        ]);

        $userPayment = $request->user();
        $userInput = $request->all();
        $userInput['user_id'] = $userPayment->id;
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paymentId = Payment::find($id);
        return response()->json($paymentId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paymentEdit = Payment::findOrFail($id);
        return view('payment', compact('paymentEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paymentUpdate = Payment::find($id);

        $paymentUpdate->user_id     = $request->user()->id;
        $paymentUpdate->qty         = $request->qty;
        $paymentUpdate->product_id  = $request->product_id;

        $paymentUpdate->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Payment::destroy($id);
        return back();
    }
}
