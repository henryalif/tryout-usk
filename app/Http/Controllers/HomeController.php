<?php

namespace App\Http\Controllers;

use App\Payment;
use App\User;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::count();
        $users = User::count();
        $payment = Payment::count();

        // $allBalance = User::all()->balance;

        $userBalance = Auth::user()->balance;

        $widgetBalance = [
            'balance' => $userBalance,
        ];

        // $widgetAllBalance = [
        //     'allBalance' => $allBalance,
        // ];

        $widgetPayment = [
            'payment' => $payment,
        ];

        $widgetProduct = [
            'product' => $product,
            //...
        ];

        $widget = [
            'users' => $users,
            //...
        ];

        return view('home', compact('widget', 'widgetProduct', 'widgetPayment', 'widgetBalance'));
    }
}
