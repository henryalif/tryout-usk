<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'qty',
        'product_id'
    ];

    function product() {
        return $this->belongsTo(Product::class);
    }   
}
