<?php

namespace Database\Seeders;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrator',
            'last_name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'role' => 1,
            'class' => 'RPL'
        ]);

        User::create([
            'name' => 'Bank',
            'last_name' => 'Bank',
            'email' => 'bank@bank.com',
            'password' => Hash::make('password'),
            'role' => 2,
            'class' => 'RPL'
        ]);

        User::create([
            'name' => 'Shop',
            'last_name' => 'Shop',
            'email' => 'shop@shop.com',
            'password' => Hash::make('password'),
            'role' => 3,
            'class' => 'RPL'
        ]);

        User::create([
            'name' => 'Student',
            'last_name' => 'Student',
            'email' => 'student@student.com',
            'password' => Hash::make('password'),
            'role' => 4,
            'class' => 'BDP'
        ]);
    }
}
