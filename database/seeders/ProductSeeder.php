<?php

namespace Database\Seeders;
use App\Product;

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Milk',
            'stocks' => '12',
            'price' => '20000',
        ]);

        Product::create([
            'name' => 'Tea',
            'stocks' => '12',
            'price' => '23000',
        ]);

        Product::create([
            'name' => 'Burger',
            'stocks' => '11',
            'price' => '50000',
        ]);

        Product::create([
            'name' => 'Latte',
            'stocks' => '120',
            'price' => '28000',
        ]);

        Product::create([
            'name' => 'Rice',
            'stocks' => '121',
            'price' => '20500',
        ]);
    }
}
